/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author drid
 */
public class CSVImporter {

    File csvFile;

    CSVImporter(File file) {
        this.csvFile = file;
    }

    public List readLines() throws FileNotFoundException, IOException {
        List<VehicleData> lines = new ArrayList<VehicleData>();
        VehicleData vd;
        CSVReader reader = new CSVReader(new FileReader(this.csvFile), ',', '"', 1);
        String[] nextLine;
        int lineCounter = 0;
        while ((nextLine = reader.readNext()) != null) {
            if (nextLine[2].isEmpty()) {
                continue;
            }
            boolean newRecord=true;
            String licensePlate = sanitizeLicensePlate(nextLine[2]);
            vd = new VehicleData();
            // locate livense plate entry
            for (VehicleData vdSearch : lines) {
                if (vdSearch.getLicensePlate().equals(licensePlate)) {
                    vd = vdSearch;
                    newRecord=false;
                    break;
                }
            }
                
            List<VehicleEncounter> veList;
            lineCounter++;
            try {
                vd.setLicensePlate(licensePlate);
                String[] model = nextLine[3].split(" ");
                vd.setMake(model[0]);
                try {
                    vd.setModel(model[1]);
                } catch (Exception e) {
                };
                vd.setColor(nextLine[4]);

                // encounters
                if (newRecord) {
                    veList = new LinkedList<VehicleEncounter>();
                }
                else
                    veList=vd.getVehicleEncounterList();

                DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                // entry date
                if (!"".equals(nextLine[0].trim())) {
                    String inTime = ("".equals(nextLine[1].trim())) ? "00:00" : nextLine[1].trim();
                    VehicleEncounter ve = new VehicleEncounter();
                    try {
                        String dateString = (nextLine[0].trim() + ' ' + inTime);
                        Date timestamp = df.parse(dateString);
                        ve.setTimestamp(timestamp);
                        ve.setEncounterType("Είσοδος");
                        veList.add(ve);
                    } catch (ParseException ex) {
                        Logger.getLogger(MainForm.class.getName()).log(Level.INFO, ("Line: " + lineCounter));
                        Logger.getLogger(MainForm.class.getName()).log(Level.INFO, ex.getMessage());
                    }
                }

                // exit date
                if (!"".equals(nextLine[6].trim()) && !"".equals(nextLine[0].trim())) {
                    VehicleEncounter ve = new VehicleEncounter();
                    // TODO add 1 day if exit h < entry h
                    try {
                        String dateString = (nextLine[0].trim() + ' ' + nextLine[6].trim());
                        Date timestamp = df.parse(dateString);
                        ve.setTimestamp(timestamp);
                        ve.setEncounterType("Έξοδος");
                        veList.add(ve);
                    } catch (ParseException ex) {
                        Logger.getLogger(MainForm.class.getName()).log(Level.INFO, ("Line: " + lineCounter));
                        Logger.getLogger(MainForm.class.getName()).log(Level.INFO, ex.getMessage());
                    }

                }
                // pack
                vd.setVehicleEncounterList(veList);
                if (newRecord) {
                    lines.add(vd);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println("Red CSV");
        return lines;
    }

    public String sanitizeLicensePlate(String plate) {
        plate = plate.toUpperCase().trim();
        plate = plate.replaceAll("ΑΆ", "A");
        plate = plate.replaceAll("Β", "B");
//        plate = plate.replaceAll("Γ", "");
//        plate = plate.replaceAll("Δ", "");
        plate = plate.replaceAll("ΕΈ", "E");
        plate = plate.replaceAll("Ζ", "Z");
        plate = plate.replaceAll("ΗΉ", "H");
//        plate = plate.replaceAll("Θ", "");
        plate = plate.replaceAll("ΙΊ", "I");
        plate = plate.replaceAll("Κ", "K");
//        plate = plate.replaceAll("Λ", "");
        plate = plate.replaceAll("Μ", "M");
        plate = plate.replaceAll("Ν", "N");
//        plate = plate.replaceAll("Ξ", "");
        plate = plate.replaceAll("ΟΌ", "O");
//        plate = plate.replaceAll("Π", "");
        plate = plate.replaceAll("Ρ", "P");
//        plate = plate.replaceAll("Σ", "");
        plate = plate.replaceAll("Τ", "T");
        plate = plate.replaceAll("ΥΎ", "Y");
//        plate = plate.replaceAll("Φ", "");
        plate = plate.replaceAll("Χ", "X");
//        plate = plate.replaceAll("Ψ", "");
//        plate = plate.replaceAll("ΩΏ", "");

        plate = plate.replaceAll("(?!([0-9a-zA-Z]|\\?|[ΔδΣσ])).", "");
//        plate = plate.re
        return plate;
    }
}
