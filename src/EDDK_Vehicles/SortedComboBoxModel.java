/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

/**
 *
 * @author drid
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.*;
import javax.swing.*;

public class SortedComboBoxModel extends DefaultComboBoxModel {

    private Comparator comparator;
    private String fName;

    /*
     * Static method is required to make sure the data is in sorted order before
     * it is added to the model
     */
    @SuppressWarnings("unchecked")
    protected static Vector sortVector(Vector items, Comparator comparator) {
        Collections.sort(items, comparator);
        return items;
    }

    /*
     * Static method is required to make sure the data is in sorted order before
     * it is added to the model
     */
    @SuppressWarnings("unchecked")
    protected static Object[] sortArray(Object[] items, Comparator comparator) {
        Arrays.sort(items, comparator);
        return items;
    }

    /*
     * Create an empty model that will use the natural sort order of the item
     */
    public SortedComboBoxModel() {
        super();
    }

    /*
     * Create an empty model that will use the specified Comparator
     */
    public SortedComboBoxModel(Comparator comparator) {
        super();
        this.comparator = comparator;
    }

    /*
     * Create a model with data and use the nature sort order of the items
     */
    public SortedComboBoxModel(Object[] items) {
        super(sortArray(items, null));
    }

    /*
     * Create a model with data and use the specified Comparator
     */
    public SortedComboBoxModel(Object[] items, Comparator comparator) {
        super(sortArray(items, comparator));
        this.comparator = comparator;
    }

    /*
     * Create a model with data and use the nature sort order of the items
     */
    public SortedComboBoxModel(Vector items) {
        this(items, null);
    }

    /*
     * Create a model with data and use the specified Comparator
     */
    public SortedComboBoxModel(Vector items, Comparator comparator) {
        super(sortVector(items, comparator));
        this.comparator = comparator;
    }

    @Override
    public void addElement(Object element) {
        if (elementExists(element)) {
            return;
        }
        insertElementAt(element, 0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void insertElementAt(Object element, int index) {
        int size = getSize();

        //  Determine where to insert element to keep model in sorted order

        for (index = 0; index < size; index++) {
            if (comparator != null) {
                Object o = getElementAt(index);

                if (comparator.compare(o, element) > 0) {
                    break;
                }
            } else {
                Comparable c = (Comparable) getElementAt(index);

                if (c.compareTo(element) > 0) {
                    break;
                }
            }
        }

        super.insertElementAt(element, index);
    }

    public boolean loadFromFile() {
        try {
            File f = new File(fName);
            if (!f.exists()) {
                return false;
            }
            FileInputStream fStream = new FileInputStream(f);
            ObjectInput stream = new ObjectInputStream(fStream);
            ArrayList<String> items = new ArrayList<String>();
            Object obj = stream.readObject();
            if (obj instanceof ArrayList) {
                this.removeAllElements();
                items = (ArrayList<String>) obj;
                for (String item : items) {
                    this.addElement(item);
                }
            }
            stream.close();
            fStream.close();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e.toString());
        }
        return true;
    }

    public boolean saveToFile() {
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < this.getSize(); i++) {
            items.add(this.getElementAt(i).toString());
        }
        try {
            FileOutputStream fStream = new FileOutputStream(fName);
            ObjectOutput stream = new ObjectOutputStream(fStream);

            stream.writeObject(items);

            stream.flush();
            stream.close();
            fStream.close();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e.toString());
        }
        return true;
    }

    public void setFileName(String fName) {
        this.fName = fName;
    }

    public boolean elementExists(Object element) {
        boolean exists = false;
        for (int index = 0; index < this.getSize() && !exists; index++) {
            if (element.equals(this.getElementAt(index))) {
                exists = true;
            }
        }
        return exists;
    }
}
