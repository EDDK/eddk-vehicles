/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author drid
 */
@Entity
@Table(name = "LOCATIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locations.findAll", query = "SELECT l FROM Locations l"),
    @NamedQuery(name = "Locations.findByLid", query = "SELECT l FROM Locations l WHERE l.lid = :lid"),
    @NamedQuery(name = "Locations.findByLocationName", query = "SELECT l FROM Locations l WHERE l.locationName = :locationName"),
    @NamedQuery(name = "Locations.findByLatitude", query = "SELECT l FROM Locations l WHERE l.latitude = :latitude"),
    @NamedQuery(name = "Locations.findByLongitude", query = "SELECT l FROM Locations l WHERE l.longitude = :longitude")})
public class Locations implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LID")
    private Integer lid;
    @Column(name = "LOCATION_NAME")
    private String locationName;
    @Column(name = "LATITUDE")
    private BigInteger latitude;
    @Column(name = "LONGITUDE")
    private BigInteger longitude;
    @OneToMany(mappedBy = "lid")
    private Collection<VehicleEncounter> vehicleEncounterCollection;

    public Locations() {
    }

    public Locations(Integer lid) {
        this.lid = lid;
    }

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        Integer oldLid = this.lid;
        this.lid = lid;
        changeSupport.firePropertyChange("lid", oldLid, lid);
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        String oldLocationName = this.locationName;
        this.locationName = locationName;
        changeSupport.firePropertyChange("locationName", oldLocationName, locationName);
    }

    public BigInteger getLatitude() {
        return latitude;
    }

    public void setLatitude(BigInteger latitude) {
        BigInteger oldLatitude = this.latitude;
        this.latitude = latitude;
        changeSupport.firePropertyChange("latitude", oldLatitude, latitude);
    }

    public BigInteger getLongitude() {
        return longitude;
    }

    public void setLongitude(BigInteger longitude) {
        BigInteger oldLongitude = this.longitude;
        this.longitude = longitude;
        changeSupport.firePropertyChange("longitude", oldLongitude, longitude);
    }

    @XmlTransient
    public Collection<VehicleEncounter> getVehicleEncounterCollection() {
        return vehicleEncounterCollection;
    }

    public void setVehicleEncounterCollection(Collection<VehicleEncounter> vehicleEncounterCollection) {
        this.vehicleEncounterCollection = vehicleEncounterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lid != null ? lid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locations)) {
            return false;
        }
        Locations other = (Locations) object;
        if ((this.lid == null && other.lid != null) || (this.lid != null && !this.lid.equals(other.lid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eddk2.Locations[ lid=" + lid + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
