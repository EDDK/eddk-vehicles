/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author drid
 */
@Entity
@Table(name = "VEHICLE_DATA", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "VehicleData.findAll", query = "SELECT v FROM VehicleData v"),
    @NamedQuery(name = "VehicleData.findByVid", query = "SELECT v FROM VehicleData v WHERE v.vid = :vid"),
    @NamedQuery(name = "VehicleData.findByLicensePlate", query = "SELECT v FROM VehicleData v WHERE v.licensePlate = :licensePlate"),
    @NamedQuery(name = "VehicleData.findByMake", query = "SELECT v FROM VehicleData v WHERE v.make = :make"),
    @NamedQuery(name = "VehicleData.findByModel", query = "SELECT v FROM VehicleData v WHERE v.model = :model")})
public class VehicleData implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VID", nullable = false)
    private Integer vid;
    @Column(name = "LICENSE_PLATE", length = 10)
    private String licensePlate;
    @Column(name = "VEHICLE_TYPE", length = 20)
    private String vehicle_type;
    @Column(name = "COLOR", length = 20)
    private String color;
    @Column(name = "MAKE", length = 255)
    private String make;
    @Column(name = "MODEL", length = 255)
    private String model;
    @Lob
    @Column(name = "OWNER_NAME")
    private String ownerName;
    @OneToMany(mappedBy = "vid", cascade=CascadeType.ALL)
    private List<VehicleEncounter> vehicleEncounterList;

    public VehicleData() {
    }

    public VehicleData(Integer vid) {
        this.vid = vid;
    }

    public Integer getVid() {
        return vid;
    }

    public void setVid(Integer vid) {
        Integer oldVid = this.vid;
        this.vid = vid;
        changeSupport.firePropertyChange("vid", oldVid, vid);
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        String oldLicensePlate = this.licensePlate;
        this.licensePlate = licensePlate;
        changeSupport.firePropertyChange("licensePlate", oldLicensePlate, licensePlate);
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        String oldMake = this.make;
        this.make = make;
        changeSupport.firePropertyChange("make", oldMake, make);
    }
    public String getVehicleType() {
        return vehicle_type;
    }

    public void setVehicleType(String vehicle_type) {
        String old_vehicle_type = this.vehicle_type;
        this.vehicle_type = vehicle_type;
        changeSupport.firePropertyChange("make", old_vehicle_type, vehicle_type);
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        String oldColor = this.color;
        this.color = color;
        changeSupport.firePropertyChange("color", oldColor, color);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        String oldModel = this.model;
        this.model = model;
        changeSupport.firePropertyChange("model", oldModel, model);
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        String oldOwnerName = this.ownerName;
        this.ownerName = ownerName;
        changeSupport.firePropertyChange("ownerName", oldOwnerName, ownerName);
    }

    public List<VehicleEncounter> getVehicleEncounterList() {
        return vehicleEncounterList;
    }

    public void setVehicleEncounterList(List<VehicleEncounter> vehicleEncounterList) {
        this.vehicleEncounterList = vehicleEncounterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vid != null ? vid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleData)) {
            return false;
        }
        VehicleData other = (VehicleData) object;
        if ((this.vid == null && other.vid != null) || (this.vid != null && !this.vid.equals(other.vid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eddk2.VehicleData[ vid=" + vid + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
