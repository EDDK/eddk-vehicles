/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author drid
 */
public class MainForm extends javax.swing.JFrame {

    private final DefaultTableCellRenderer locationCellRenderer;
    private final org.jdesktop.beansbinding.Converter<String, String> cvToUpper;

    /**
     * Creates new form MainForm
     */
    public MainForm() {
        try {
            UIManager.setLookAndFeel(
               UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dateCellRenderer = new DefaultTableCellRenderer() {
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yy - HH:mm");

            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                if (value instanceof Date) {
                    value = f.format(value);
                }
                return super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);
            }
        };

        this.locationCellRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                if (value instanceof Locations) {
                    Locations L = (Locations) value;
                    value = L.getLocationName();
                }
                return super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);
            }
        };

        this.cvToUpper = new org.jdesktop.beansbinding.Converter<String, String>() {
            @Override
            public String convertForward(String value) {
                return value.toUpperCase();
            }

            @Override
            public String convertReverse(String value) {
                return value.toUpperCase();
            }
        };

        initComponents();

        tblVehicleEncounter.getRowSorter().toggleSortOrder(0);


        // Selection listener
        tblVehicleData.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
//                System.out.println(e);
                int row = tblVehicleData.getSelectedRow();
                if (row >= 0) {
                    int vdIndex = tblVehicleData.getSelectedRow();
                    vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(vdIndex));
                    if (vd.getVehicleType().equals(lastVType)) {
                        cbManufacturer.setSelectedItem(vd.getMake().toUpperCase());
                    }
                    lastVType = vd.getVehicleType();
                    btVENewRecord.setEnabled(true);
                    // disable VD form elments
                    cbVehicleType.setEnabled(false);
                    btVDSave.setText("Επεξεργασια");
                    btVDSave.setEnabled(true);
                } else {
                    btVENewRecord.setEnabled(false);
                    // Enable VD form elments
                    cbVehicleType.setEnabled(true);
                    btVDSave.setText("Καταχώρηση");

                }
            }
        });

        // initialize manufacturers
        try {
            SortedComboBoxModel model;
            parseXML();
            vehicleMakeCar.setFileName("mfCars.obj");
            vehicleMakeMotorcycle.setFileName("mfMotorycles.obj");
            vehicleMakeTruck.setFileName("mfTrucks.obj");
            vehicleMakeCar.loadFromFile();
            vehicleMakeMotorcycle.loadFromFile();
            vehicleMakeTruck.loadFromFile();
            cbManufacturer.setModel(vehicleMakeCar);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Set log to file
        try {
            newErr = new PrintStream("error.log");
            System.setErr(newErr);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        em = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory("EDDK_VEHICLESPU").createEntityManager();
        vehicleDataQuery = java.beans.Beans.isDesignTime() ? null : em.createQuery("SELECT v FROM VehicleData v WHERE v.licensePlate  LIKE :licensePlate");
        vehicleDataQuery.setParameter("licensePlate", "%");
        vehicleData = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(vehicleDataQuery.getResultList());
        locationsQuery = java.beans.Beans.isDesignTime() ? null : em.createQuery("SELECT l FROM Locations l ORDER BY l.locationName");
        locations = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(locationsQuery.getResultList());
        jFileChooser1 = new javax.swing.JFileChooser();
        vdEditMode = new EDDK_Vehicles.EditMode();
        fileSelectorRestore = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        btClearSearch = new javax.swing.JButton();
        tfLicensePlate = new javax.swing.JTextField();
        DocumentFilter filter = new UppercaseDocumentFilter();
        ((AbstractDocument) tfLicensePlate.getDocument()).setDocumentFilter(filter);
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfOwner = new javax.swing.JTextField();
        ((AbstractDocument) tfOwner.getDocument()).setDocumentFilter(filter);
        cbManufacturer = new javax.swing.JComboBox();
        tfModel = new javax.swing.JTextField();
        ((AbstractDocument) tfModel.getDocument()).setDocumentFilter(filter);
        btVDSave = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        tfColor = new javax.swing.JTextField();
        ((AbstractDocument) tfColor.getDocument()).setDocumentFilter(filter);
        jLabel12 = new javax.swing.JLabel();
        cbVehicleType = new javax.swing.JComboBox();
        tbNewModel = new javax.swing.JToggleButton();
        btVDDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cbEncounterType = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        taNotes = new javax.swing.JTextArea();
        cbLocationName = new javax.swing.JComboBox();
        tfReason = new javax.swing.JTextField();
        tfAuthority = new javax.swing.JTextField();
        tfDate = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tfTime = new javax.swing.JFormattedTextField();
        btVEDelete = new javax.swing.JButton();
        btVECancel = new javax.swing.JButton();
        btVESave = new javax.swing.JButton();
        btVENewRecord = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        btVEEdit = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblVehicleData = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblVehicleEncounter = new javax.swing.JTable();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        menuCSVImport = new javax.swing.JMenuItem();
        menuCSVExport = new javax.swing.JMenuItem();
        menuBackup = new javax.swing.JMenuItem();
        menuRestoreDB = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        menuSettings = new javax.swing.JMenu();
        meniLocations = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        vdEditMode.setEditEncounterEnabled(java.lang.Boolean.FALSE);
        vdEditMode.setEditVehicleEnabled(java.lang.Boolean.FALSE);

        fileSelectorRestore.setCurrentDirectory(new java.io.File("/home/drid/BACKUP"));
        fileSelectorRestore.setDialogTitle("Επιλέξτε αντίγραφο ασφαλείας");
        fileSelectorRestore.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ΕΔΔΚ Οχήματα");
        setMinimumSize(new java.awt.Dimension(650, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Όχημα"));

        jLabel4.setText("Ιδιοκτήτης");

        btClearSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/clear-icon-30.png"))); // NOI18N
        btClearSearch.setBorderPainted(false);
        btClearSearch.setContentAreaFilled(false);
        btClearSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClearSearchActionPerformed(evt);
            }
        });

        tfLicensePlate.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${!enabled}"), tfLicensePlate, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        tfLicensePlate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfLicensePlateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfLicensePlateKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jLabel1.setText("Αρ.Κυκλοφορίας");

        jLabel3.setText("Μοντέλο");

        jLabel2.setText("Μάρκα");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, tblVehicleData, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.ownerName}"), tfOwner, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, cbVehicleType, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfOwner, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tbNewModel, org.jdesktop.beansbinding.ELProperty.create("${selected}"), cbManufacturer, org.jdesktop.beansbinding.BeanProperty.create("editable"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, cbVehicleType, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), cbManufacturer, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, tblVehicleData, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.model}"), tfModel, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, cbVehicleType, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfModel, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVDSave.setText("Καταχώρηση");
        btVDSave.setEnabled(false);
        btVDSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVDSaveActionPerformed(evt);
            }
        });

        jLabel11.setText("Χρώμα");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, tblVehicleData, org.jdesktop.beansbinding.ELProperty.create("${selectedElement_IGNORE_ADJUSTING.color}"), tfColor, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, cbVehicleType, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfColor, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jLabel12.setText("Τύπος");

        cbVehicleType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Αυτοκίνητο", "Δίκυκλο", "Φορτηγό" }));

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, tblVehicleData, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.vehicleType}"), cbVehicleType, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);

        cbVehicleType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbVehicleTypeActionPerformed(evt);
            }
        });

        tbNewModel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/plus-20.png"))); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, cbVehicleType, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tbNewModel, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVDDelete.setText("Διαγραφή");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleData, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), btVDDelete, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVDDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVDDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfLicensePlate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btClearSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfOwner)
                            .addComponent(tfColor)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cbManufacturer, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tbNewModel))
                            .addComponent(tfModel, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btVDDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btVDSave, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbVehicleType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel1))
                    .addComponent(tfLicensePlate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btClearSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbVehicleType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tbNewModel)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbManufacturer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btVDSave)
                    .addComponent(btVDDelete))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Κίνηση"));

        jLabel6.setText("Χρονοσήμανση");

        jLabel9.setText("Ιδιότητα");

        cbEncounterType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Είσοδος", "Έξοδος", "Διέλευση", "Στάθμευση" }));

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.encounterType}"), cbEncounterType, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), cbEncounterType, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jLabel10.setText("Σημειώσεις");

        jLabel5.setText("Κίνηση");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });

        taNotes.setColumns(20);
        taNotes.setRows(5);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.notes}"), taNotes, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), taNotes, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jScrollPane3.setViewportView(taNotes);

        cbLocationName.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(
                JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof Locations) {
                    Locations L = (Locations)value;
                    setText(L.getLocationName());
                }
                return this;
            }
        });

        org.jdesktop.swingbinding.JComboBoxBinding jComboBoxBinding = org.jdesktop.swingbinding.SwingBindings.createJComboBoxBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, locations, cbLocationName);
        bindingGroup.addBinding(jComboBoxBinding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.lid}"), cbLocationName, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), cbLocationName, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.reason}"), tfReason, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfReason, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.driversAuthority}"), tfAuthority, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);
        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfAuthority, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        ((AbstractDocument) tfAuthority.getDocument()).setDocumentFilter(filter);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfDate, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jLabel8.setText("Αιτιολογία");

        jLabel7.setText("Θέση");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), tfTime, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVEDelete.setText("Διαγραφή");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), btVEDelete, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVEDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVEDeleteActionPerformed(evt);
            }
        });

        btVECancel.setText("Ακύρωση");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), btVECancel, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVECancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVECancelActionPerformed(evt);
            }
        });

        btVESave.setText("Καταχώρηση");
        btVESave.setEnabled(false);
        btVESave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVESaveActionPerformed(evt);
            }
        });

        btVENewRecord.setText("Νέα");
        btVENewRecord.setEnabled(false);
        btVENewRecord.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVENewRecordActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/plus-20.png"))); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), jButton4, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        btVEEdit.setText("Επεξεργασια");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleEncounter, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), btVEEdit, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        btVEEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVEEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btVENewRecord)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btVESave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btVECancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btVEEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btVEDelete))
                            .addComponent(jScrollPane3)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(cbLocationName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton4))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(tfDate, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfTime))
                                    .addComponent(cbEncounterType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tfReason)
                                    .addComponent(tfAuthority))))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbEncounterType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(tfDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(cbLocationName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(tfReason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(tfAuthority, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btVEDelete)
                    .addComponent(btVECancel)
                    .addComponent(btVESave)
                    .addComponent(btVENewRecord)
                    .addComponent(btVEEdit))
                .addContainerGap())
        );

        tblVehicleData.setAutoCreateRowSorter(true);
        tblVehicleData.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, vehicleData, tblVehicleData);
        org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${licensePlate}"));
        columnBinding.setColumnName("Αρ.Κυκλ.");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${make}"));
        columnBinding.setColumnName("Μάρκα");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${model}"));
        columnBinding.setColumnName("Μοντέλο");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${vehicleType}"));
        columnBinding.setColumnName("Τύπος");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${color}"));
        columnBinding.setColumnName("Χρώμα");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${ownerName}"));
        columnBinding.setColumnName("Ιδιοκτήτης");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, vdEditMode, org.jdesktop.beansbinding.ELProperty.create("${!editVehicleEnabled && !editEncounterEnabled}"), tblVehicleData, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jScrollPane1.setViewportView(tblVehicleData);

        tblVehicleEncounter.setAutoCreateRowSorter(true);
        tblVehicleEncounter.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        org.jdesktop.beansbinding.ELProperty eLProperty = org.jdesktop.beansbinding.ELProperty.create("${selectedElement.vehicleEncounterList}");
        jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, tblVehicleData, eLProperty, tblVehicleEncounter);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${timestamp}"));
        columnBinding.setColumnName("Χρόνος");
        columnBinding.setColumnClass(java.util.Date.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${lid}"));
        columnBinding.setColumnName("Θέση");
        columnBinding.setColumnClass(EDDK_Vehicles.Locations.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${encounterType}"));
        columnBinding.setColumnName("Κίνηση");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${reason}"));
        columnBinding.setColumnName("Αιτιολογία");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${driversAuthority}"));
        columnBinding.setColumnName("Drivers Authority");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${notes}"));
        columnBinding.setColumnName("Σημειώσεις");
        columnBinding.setColumnClass(String.class);
        columnBinding.setEditable(false);
        jTableBinding.setSourceUnreadableValue(java.util.Collections.emptyList());
        bindingGroup.addBinding(jTableBinding);
        jTableBinding.bind();binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btVESave, org.jdesktop.beansbinding.ELProperty.create("${!enabled}"), tblVehicleEncounter, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jScrollPane2.setViewportView(tblVehicleEncounter);
        if (tblVehicleEncounter.getColumnModel().getColumnCount() > 0) {
            tblVehicleEncounter.getColumnModel().getColumn(0).setCellRenderer(dateCellRenderer);
            tblVehicleEncounter.getColumnModel().getColumn(1).setCellRenderer(locationCellRenderer);
        }

        fileMenu.setMnemonic('f');
        fileMenu.setText("File");

        menuCSVImport.setMnemonic('s');
        menuCSVImport.setText("Εισαγωγή CSV");
        menuCSVImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCSVImportActionPerformed(evt);
            }
        });
        fileMenu.add(menuCSVImport);

        menuCSVExport.setMnemonic('a');
        menuCSVExport.setText("Εξαγωγή CSV");
        menuCSVExport.setEnabled(false);
        fileMenu.add(menuCSVExport);

        menuBackup.setText("Backup");
        menuBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBackupActionPerformed(evt);
            }
        });
        fileMenu.add(menuBackup);

        menuRestoreDB.setText("Restore");
        menuRestoreDB.setEnabled(false);
        menuRestoreDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRestoreDBActionPerformed(evt);
            }
        });
        fileMenu.add(menuRestoreDB);

        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Έξοδος");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        menuSettings.setMnemonic('e');
        menuSettings.setText("Ρυθμίσεις");

        meniLocations.setMnemonic('t');
        meniLocations.setText("Τοποθεσίες");
        meniLocations.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                meniLocationsActionPerformed(evt);
            }
        });
        menuSettings.add(meniLocations);

        menuBar.add(menuSettings);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Βοήθεια");

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("Σχετικά");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
                .addContainerGap())
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void btVENewRecordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVENewRecordActionPerformed
        tblVehicleEncounter.clearSelection();
        tfAuthority.setText(null);
        tfReason.setText(null);
        taNotes.setText(null);
        btVENewRecord.setEnabled(false);
        btVESave.setEnabled(true);
        setCurrentDate();
    }//GEN-LAST:event_btVENewRecordActionPerformed

    private void btVESaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVESaveActionPerformed
        em.getTransaction().begin();
        VehicleEncounter ve;
        int index = tblVehicleData.getSelectedRow();
        if (vdEditMode.getEditEncounterEnabled() == false) {
            ve = new VehicleEncounter();
            ve.setDriversAuthority(tfAuthority.getText());
            ve.setEncounterType(cbEncounterType.getSelectedItem().toString());
            ve.setLocationName(cbLocationName.getSelectedItem().toString());
            ve.setNotes(taNotes.getText());
            ve.setReason(tfReason.getText());
            Locations L = (Locations) cbLocationName.getSelectedItem();
            ve.setLid(L);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            try {
                Date timestamp = df.parse(tfDate.getText() + ' ' + tfTime.getText());
                ve.setTimestamp(timestamp);
            } catch (ParseException ex) {
                Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(index));

            Collection<EDDK_Vehicles.VehicleEncounter> veList = vd.getVehicleEncounterList();

            if (veList == null) {
                veList = new LinkedList<EDDK_Vehicles.VehicleEncounter>();
                vd.setVehicleEncounterList((List<VehicleEncounter>) veList);
            }

            ve.setVid(vd);
            em.persist(ve);
            veList.add(ve);
        } else {
            vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(index));
            int veIndex = tblVehicleEncounter.getSelectedRow();
            ve = vd.getVehicleEncounterList().get(tblVehicleEncounter.convertRowIndexToModel(veIndex));
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            try {
                Date timestamp = df.parse(tfDate.getText() + ' ' + tfTime.getText());
                ve.setTimestamp(timestamp);
            } catch (ParseException ex) {
                Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            em.persist(ve);
        }
        em.getTransaction().commit();
        searchVehicle();
        tblVehicleData.clearSelection();
        tblVehicleData.setRowSelectionInterval(index, index);
//        int row = veList.size() - 1;
//        tblVehicleEncounter.setRowSelectionInterval(row, row);
//        tblVehicleEncounter.scrollRectToVisible(tblVehicleEncounter.getCellRect(row, 0, true));
        vdEditMode.setEditEncounterEnabled(false);
        btVESave.setEnabled(false);
        btVENewRecord.setEnabled(true);

    }//GEN-LAST:event_btVESaveActionPerformed

    private void btVECancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVECancelActionPerformed
        vdEditMode.setEditEncounterEnabled(false);
        btVESave.setEnabled(false);
        btVENewRecord.setEnabled(true);
    }//GEN-LAST:event_btVECancelActionPerformed

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        showLocationsForm();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void meniLocationsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_meniLocationsActionPerformed
        showLocationsForm();
    }//GEN-LAST:event_meniLocationsActionPerformed

    private void menuCSVImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCSVImportActionPerformed
        int returnVal = jFileChooser1.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser1.getSelectedFile();
            try {
                CSVImporter ci = new CSVImporter(file);
                List data = ci.readLines();

                // insert to db
                em.getTransaction().begin();
                for (Iterator it = data.iterator(); it.hasNext();) {
                    VehicleData entity = (VehicleData) it.next();
                    em.persist(entity);
                    vehicleData.add(entity);
                }
                em.getTransaction().commit();
            } catch (IOException ioe) {
                System.out.println("problem accessing file" + file.getAbsolutePath());
            }
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_menuCSVImportActionPerformed

    private void btVEDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVEDeleteActionPerformed
        int n = JOptionPane.showConfirmDialog(this, "Να διαγραφεί η διέλευση;\n",
                "Διαγραφή διέλευσης",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (n == JOptionPane.YES_OPTION) {
            int vdIndex = tblVehicleData.getSelectedRow();
            vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(vdIndex));
            int index = tblVehicleEncounter.getSelectedRow();
            VehicleEncounter ve = vd.getVehicleEncounterList().get(tblVehicleEncounter.convertRowIndexToModel(index));
            em.getTransaction().begin();
            em.remove(ve);
            vd.getVehicleEncounterList().remove(tblVehicleEncounter.convertRowIndexToModel(index));
            em.getTransaction().commit();
//        em.persist(vd);
            tblVehicleData.clearSelection();
            tblVehicleData.setRowSelectionInterval(vdIndex, vdIndex);
            btVECancelActionPerformed(null);
        }
    }//GEN-LAST:event_btVEDeleteActionPerformed

    private void btVEEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVEEditActionPerformed
        int vdIndex = tblVehicleData.getSelectedRow();
        vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(vdIndex));
        int index = tblVehicleEncounter.getSelectedRow();
        VehicleEncounter ve = vd.getVehicleEncounterList().get(tblVehicleEncounter.convertRowIndexToModel(index));
        try {
            tfDate.setText(ve.getDate());
            tfTime.setText(ve.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        btVENewRecord.setEnabled(false);
        btVESave.setEnabled(true);
        vdEditMode.setEditEncounterEnabled(true);
    }//GEN-LAST:event_btVEEditActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        vehicleMakeCar.saveToFile();
        vehicleMakeMotorcycle.saveToFile();
        vehicleMakeTruck.saveToFile();
    }//GEN-LAST:event_formWindowClosing

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        AboutForm af = new AboutForm(this, true);
        af.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void menuBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBackupActionPerformed
        SimpleDateFormat todaysDate =
                new java.text.SimpleDateFormat("yyyy-MM-dd_HHmm");
        String backupdirectory = "BACKUP/"
                + todaysDate.format((java.util.Calendar.getInstance()).getTime());
        em.getTransaction().begin();
        java.sql.Connection connection = em.unwrap(java.sql.Connection.class);
        CallableStatement cs;
        try {
            cs = connection.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
            cs.setString(1, backupdirectory);
            cs.execute();
            cs.close();
            JOptionPane.showMessageDialog(this, "Αντίγραφο ασφαλείας ολοκληρώθηκε",
                    "Aντίγραφο ασφαλείας",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Αντίγραφο ασφαλείας απέτυχε",
                    "Aντίγραφο ασφαλείας",
                    JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }

//        System.out.println("backed up database to " + backupdirectory);
        em.getTransaction().commit();
    }//GEN-LAST:event_menuBackupActionPerformed

    private void menuRestoreDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRestoreDBActionPerformed
        int fs = fileSelectorRestore.showOpenDialog(this);
        if (fs == JFileChooser.APPROVE_OPTION) {
            int confirm = JOptionPane.showConfirmDialog(this,
                    "Να γίνει ανάκτηση του αντίγραφου ασφαλείας "
                    + fileSelectorRestore.getSelectedFile().getName()
                    + "\n Η διαδικασία αυτή θα ΔΙΑΓΡΑΨΕΙ τα υπάρχοντα δεδομένα και \nθα τα αντικαταστήσει με το αντιγραφο ασφαλείας",
                    "Ανάκτηση αντίγραφου ασφαλείας",
                    JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {   
                try {
                    em.close();
                    String dbURL = "jdbc:derby:EDDK_VEHICLES;restoreFrom=" + 
                            fileSelectorRestore.getSelectedFile().getCanonicalPath();
                    java.sql.Connection conn = DriverManager.getConnection(dbURL);
                    conn.commit();
                    conn.close();
                    JOptionPane.showMessageDialog(this, "Ανάκτηση ολοκληρώθηκε",
                            "Ανάκτηση αντίγραφου ασφαλείας",
                            JOptionPane.INFORMATION_MESSAGE);
                    
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, "Ανάκτηση απέτυχε",
                            "Ανάκτηση αντίγραφου ασφαλείας",
                            JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_menuRestoreDBActionPerformed

    private void btVDDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVDDeleteActionPerformed
        int n = JOptionPane.showConfirmDialog(this, "Να διαγραφεί το όχημα;\n"
            + "Με τη διαγραφή του οχήματος διαγράφονται και οι διελεύσεις του.\n",
            "Διαγραφή οχήματος",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE);
        if (n == JOptionPane.YES_OPTION) {
            vdEditMode.setEditVehicleEnabled(false);
            int vdIndex = tblVehicleData.getSelectedRow();
            vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(vdIndex));
            em.getTransaction().begin();
            em.remove(vd);
            vehicleData.remove(tblVehicleData.convertRowIndexToModel(vdIndex));
            em.getTransaction().commit();
            vd = null;
            btClearSearchActionPerformed(null);
        }
    }//GEN-LAST:event_btVDDeleteActionPerformed

    private void cbVehicleTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbVehicleTypeActionPerformed
        switch (cbVehicleType.getSelectedIndex()) {
            case 0:
            cbManufacturer.setModel(vehicleMakeCar);
            break;
            case 1:
            cbManufacturer.setModel(vehicleMakeMotorcycle);
            break;
            case 2:
            cbManufacturer.setModel(vehicleMakeTruck);
            break;
        }
        //        cbManufacturer.setSelectedIndex(-1);
        int vdIndex = tblVehicleData.getSelectedRow();
        if (vdIndex > -1) {
            vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(vdIndex));
            cbManufacturer.setSelectedItem(vd.getMake().toUpperCase());
        }
        //        System.out.println(evt);
    }//GEN-LAST:event_cbVehicleTypeActionPerformed

    private void btVDSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVDSaveActionPerformed
        //        VehicleData vd;
        if (vdEditMode.getEditVehicleEnabled() == false) {
            if (tblVehicleData.getSelectedRow() == -1) {
                vd = new VehicleData();
            } else {
                vd = vehicleData.get(tblVehicleData.convertRowIndexToModel(tblVehicleData.getSelectedRow()));
                vdEditMode.setEditVehicleEnabled(true);
                //                btVDDelete.setEnabled(false);
                cbVehicleType.setEnabled(true);
                btVDSave.setText("Καταχώρηση");
                tfLicensePlate.setText(vd.getLicensePlate());
                return;
            }
        }

        vd.setLicensePlate(tfLicensePlate.getText());
        vd.setVehicleType(cbVehicleType.getSelectedItem().toString());
        vd.setMake(cbManufacturer.getSelectedItem().toString().toUpperCase());
        vd.setModel(tfModel.getText());
        vd.setColor(tfColor.getText());
        vd.setOwnerName(tfOwner.getText());
        em.getTransaction().begin();
        em.persist(vd);
        em.getTransaction().commit();
        // add vehicle make to list
        if (tbNewModel.isSelected()) {
            vehicleMakeCar.addElement(cbManufacturer.getSelectedItem().toString());
            //            cbManufacturer.addItem(cbManufacturer.getSelectedItem().toString());
            tbNewModel.setSelected(false);
        }
        vdEditMode.setEditVehicleEnabled(false);
        cbVehicleType.setEnabled(false);
        searchVehicle();
    }//GEN-LAST:event_btVDSaveActionPerformed

    private void tfLicensePlateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfLicensePlateKeyTyped
        // TODO add your handling code here:
        String c = String.valueOf(evt.getKeyChar());
        if (!filterText(c)) {
            evt.consume();
        }
    }//GEN-LAST:event_tfLicensePlateKeyTyped

    private void tfLicensePlateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfLicensePlateKeyReleased
        btVDSave.setEnabled(!tfLicensePlate.getText().isEmpty());
        if (!vdEditMode.getEditVehicleEnabled()) {
            searchVehicle();
        }
    }//GEN-LAST:event_tfLicensePlateKeyReleased

    private void btClearSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btClearSearchActionPerformed
        tfLicensePlate.setText("");
        btVDSave.setEnabled(false);
        vdEditMode.setEditVehicleEnabled(false);
        if (vd != null) {
            em.refresh(vd);
        }
        searchVehicle();
        resetVDForm();
    }//GEN-LAST:event_btClearSearchActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton btClearSearch;
    private javax.swing.JButton btVDDelete;
    private javax.swing.JButton btVDSave;
    private javax.swing.JButton btVECancel;
    private javax.swing.JButton btVEDelete;
    private javax.swing.JButton btVEEdit;
    private javax.swing.JButton btVENewRecord;
    private javax.swing.JButton btVESave;
    private javax.swing.JComboBox cbEncounterType;
    private javax.swing.JComboBox cbLocationName;
    private javax.swing.JComboBox cbManufacturer;
    private javax.swing.JComboBox cbVehicleType;
    private javax.persistence.EntityManager em;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JFileChooser fileSelectorRestore;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JButton jButton4;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private java.util.List locations;
    private javax.persistence.Query locationsQuery;
    private javax.swing.JMenuItem meniLocations;
    private javax.swing.JMenuItem menuBackup;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuCSVExport;
    private javax.swing.JMenuItem menuCSVImport;
    private javax.swing.JMenuItem menuRestoreDB;
    private javax.swing.JMenu menuSettings;
    private javax.swing.JTextArea taNotes;
    private javax.swing.JToggleButton tbNewModel;
    private javax.swing.JTable tblVehicleData;
    private javax.swing.JTable tblVehicleEncounter;
    private javax.swing.JTextField tfAuthority;
    private javax.swing.JTextField tfColor;
    private javax.swing.JFormattedTextField tfDate;
    private javax.swing.JTextField tfLicensePlate;
    private javax.swing.JTextField tfModel;
    private javax.swing.JTextField tfOwner;
    private javax.swing.JTextField tfReason;
    private javax.swing.JFormattedTextField tfTime;
    private EDDK_Vehicles.EditMode vdEditMode;
    private java.util.List<EDDK_Vehicles.VehicleData> vehicleData;
    private javax.persistence.Query vehicleDataQuery;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
    VehicleData vd;
    TableCellRenderer dateCellRenderer;
    String lastVType = null;
//    boolean vdEditMode = false;
//    boolean veEditMode = false;
    private SortedComboBoxModel vehicleMakeCar;
    private SortedComboBoxModel vehicleMakeMotorcycle;
    private SortedComboBoxModel vehicleMakeTruck;
    PrintStream newErr;

    class UppercaseDocumentFilter extends DocumentFilter {

        @Override
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String text,
                AttributeSet attr) throws BadLocationException {
            if (!filterText(text)) {
                text = "";
            }
            fb.insertString(offset, text.toUpperCase(), attr);
        }

        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text,
                AttributeSet attrs) throws BadLocationException {
            fb.replace(offset, length, text.toUpperCase(), attrs);
        }
    }

    public boolean filterText(String text) {
        return text.matches("[0-9a-zA-Z]|\\?|[ΠπΔδΣσ]");

    }

    public void searchVehicle() {
        tblVehicleData.clearSelection();
        vehicleDataQuery.setParameter("licensePlate", tfLicensePlate.getText() + '%');
//        em.getTransaction().begin();
        java.util.Collection<VehicleData> data;
        data = vehicleDataQuery.getResultList();
//        for (Object entity : data) {
//            em.refresh(entity);
//        }
//        em.getTransaction().commit();


        vehicleData.clear();
        vehicleData.addAll(data);
        if (vehicleData.size() == 1) {
            tblVehicleData.setRowSelectionInterval(0, 0);
        } else {
            resetVDForm();
        }
    }

    private class JComponentImpl extends JComponent {

        public JComponentImpl() {
        }
        public boolean enabled;

        @Override
        public void enable() {
            super.enable();
            this.enabled = true;
        }

        @Override
        public void enable(boolean b) {
            super.enable(b);
            this.enabled = b;
        }
    }

    public void setCurrentDate() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        tfDate.setText(dateFormat.format(date));
        tfTime.setText(timeFormat.format(date));
    }

    public void showLocationsForm() {
        final JFrame locationDialog = new JFrame();
        final LocationsForm lf = new LocationsForm(locationDialog);

        locationDialog.setLocationRelativeTo(this);

        locationDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        locationDialog.add(lf);

        locationDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                lf.saveDataset();
                locationDialog.dispose();
                setEnabled(true);
                locations.clear();
                locations.addAll(locationsQuery.getResultList());
            }
        });

        this.setEnabled(false);

        locationDialog.pack();
        locationDialog.setVisible(true);
    }

    public String getDate(Date timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(timestamp);
    }

    public String getTime(Date timestamp) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(timestamp);
    }

    private void resetVDForm() {
        cbVehicleType.setSelectedIndex(0);
        cbManufacturer.setSelectedIndex(-1);
        tfModel.setText(null);
        tfColor.setText(null);
        tfOwner.setText(null);
    }

    private void parseXML() throws ParserConfigurationException, IOException, SAXException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        final ArrayList<String> motorcycle = new ArrayList<String>();
        final ArrayList<String> car = new ArrayList<String>();
        final ArrayList<String> truck = new ArrayList<String>();
        DefaultHandler handler = new DefaultHandler() {
            String vType;
            boolean item = false;

            @Override
            public void startElement(String uri, String localName, String qName,
                    Attributes attributes) throws SAXException {

//		System.out.println("Start Element :" + qName);

                if (qName.equalsIgnoreCase("item")) {
                    item = true;
                } else {
                    vType = qName;
                }

            }

            @Override
            public void characters(char ch[], int start, int length) throws SAXException {
                if (item) {
//			System.out.println("vType: "+vType+" Make : " + new String(ch, start, length));
                    item = false;
                    if ("motorcycle".equals(vType)) {
                        motorcycle.add(new String(ch, start, length));
                    }
                    if ("car".equals(vType)) {
                        car.add(new String(ch, start, length));
                    }
                    if ("truck".equals(vType)) {
                        truck.add(new String(ch, start, length));
                    }
                }
            }
        };
        saxParser.parse(this.getClass().getResourceAsStream("resources/Manufacturers.xml"), handler);
        vehicleMakeCar = new SortedComboBoxModel(car.toArray());
        vehicleMakeMotorcycle = new SortedComboBoxModel(motorcycle.toArray());
        vehicleMakeTruck = new SortedComboBoxModel(truck.toArray());
    }
}
