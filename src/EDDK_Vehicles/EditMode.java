/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author drid
 */
public class EditMode implements Serializable {
    
    public static final String PROP_EDIT_VEHICLE_ENABLED = "editVehicleEnabled";
    public static final String PROP_EDIT_ENCOUNTER_ENABLED = "editEncounterEnabled";
    private Boolean editVehicleEnabled;
    private Boolean editEncounterEnabled;
    private PropertyChangeSupport propertySupport;
    
    public EditMode() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public Boolean getEditVehicleEnabled() {
        return editVehicleEnabled;
    }
    
    public void setEditVehicleEnabled(Boolean value) {
        Boolean oldValue = editVehicleEnabled;
        editVehicleEnabled = value;
        propertySupport.firePropertyChange(PROP_EDIT_VEHICLE_ENABLED, oldValue, editVehicleEnabled);
    }
    
    public Boolean getEditEncounterEnabled() {
        return editEncounterEnabled;
    }
    
    public void setEditEncounterEnabled(Boolean value) {
        Boolean oldValue = editEncounterEnabled;
        editEncounterEnabled = value;
        propertySupport.firePropertyChange(PROP_EDIT_ENCOUNTER_ENABLED, oldValue, editEncounterEnabled);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
}
