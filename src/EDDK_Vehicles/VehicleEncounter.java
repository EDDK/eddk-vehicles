/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EDDK_Vehicles;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author drid
 */
@Entity
@Table(name = "VEHICLE_ENCOUNTER", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "VehicleEncounter.findAll", query = "SELECT v FROM VehicleEncounter v"),
    @NamedQuery(name = "VehicleEncounter.findByVeid", query = "SELECT v FROM VehicleEncounter v WHERE v.veid = :veid"),
    @NamedQuery(name = "VehicleEncounter.findByDriversAuthority", query = "SELECT v FROM VehicleEncounter v WHERE v.driversAuthority = :driversAuthority"),
    @NamedQuery(name = "VehicleEncounter.findByEncounterType", query = "SELECT v FROM VehicleEncounter v WHERE v.encounterType = :encounterType"),
    @NamedQuery(name = "VehicleEncounter.findByLocationLat", query = "SELECT v FROM VehicleEncounter v WHERE v.locationLat = :locationLat"),
    @NamedQuery(name = "VehicleEncounter.findByLocationLon", query = "SELECT v FROM VehicleEncounter v WHERE v.locationLon = :locationLon"),
    @NamedQuery(name = "VehicleEncounter.findByLocationName", query = "SELECT v FROM VehicleEncounter v WHERE v.locationName = :locationName"),
    @NamedQuery(name = "VehicleEncounter.findByTimestamp", query = "SELECT v FROM VehicleEncounter v WHERE v.timestamp = :timestamp"),
    @NamedQuery(name = "VehicleEncounter.findByLid", query = "SELECT v FROM VehicleEncounter v WHERE v.lid = :lid")})
public class VehicleEncounter implements Serializable {
    @JoinColumn(name = "LID", referencedColumnName = "LID")
    @ManyToOne
    private Locations lid;
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "VEID", nullable = false)
    private Integer veid;
    @Column(name = "DRIVERS_AUTHORITY", length = 255)
    private String driversAuthority;
    @Column(name = "ENCOUNTER_TYPE", length = 255)
    private String encounterType;
    @Column(name = "LOCATION_LAT")
    private Integer locationLat;
    @Column(name = "LOCATION_LON")
    private Integer locationLon;
    @Column(name = "LOCATION_NAME", length = 255)
    private String locationName;
    @Lob
    @Column(name = "NOTES")
    private String notes;
    @Lob
    @Column(name = "REASON")
    private String reason;
    @Column(name = "TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    @JoinColumn(name = "VID", referencedColumnName = "VID")
    @ManyToOne
    private VehicleData vid;

    public VehicleEncounter() {
    }

    public VehicleEncounter(Integer veid) {
        this.veid = veid;
    }

    public Integer getVeid() {
        return veid;
    }

    public void setVeid(Integer veid) {
        Integer oldVeid = this.veid;
        this.veid = veid;
        changeSupport.firePropertyChange("veid", oldVeid, veid);
    }

    public String getDriversAuthority() {
        return driversAuthority;
    }

    public void setDriversAuthority(String driversAuthority) {
        String oldDriversAuthority = this.driversAuthority;
        this.driversAuthority = driversAuthority;
        changeSupport.firePropertyChange("driversAuthority", oldDriversAuthority, driversAuthority);
    }

    public String getEncounterType() {
        return encounterType;
    }

    public void setEncounterType(String encounterType) {
        String oldEncounterType = this.encounterType;
        this.encounterType = encounterType;
        changeSupport.firePropertyChange("encounterType", oldEncounterType, encounterType);
    }

    public Integer getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(Integer locationLat) {
        Integer oldLocationLat = this.locationLat;
        this.locationLat = locationLat;
        changeSupport.firePropertyChange("locationLat", oldLocationLat, locationLat);
    }

    public Integer getLocationLon() {
        return locationLon;
    }

    public void setLocationLon(Integer locationLon) {
        Integer oldLocationLon = this.locationLon;
        this.locationLon = locationLon;
        changeSupport.firePropertyChange("locationLon", oldLocationLon, locationLon);
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        String oldLocationName = this.locationName;
        this.locationName = locationName;
        changeSupport.firePropertyChange("locationName", oldLocationName, locationName);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        String oldNotes = this.notes;
        this.notes = notes;
        changeSupport.firePropertyChange("notes", oldNotes, notes);
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        String oldReason = this.reason;
        this.reason = reason;
        changeSupport.firePropertyChange("reason", oldReason, reason);
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        Date oldTimestamp = this.timestamp;
        this.timestamp = timestamp;
        changeSupport.firePropertyChange("timestamp", oldTimestamp, timestamp);
    }

    public VehicleData getVid() {
        return vid;
    }

    public void setVid(VehicleData vid) {
        VehicleData oldVid = this.vid;
        this.vid = vid;
        changeSupport.firePropertyChange("vid", oldVid, vid);
    }

    public String getDate() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(timestamp);
    }
    
    public String getTime() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(timestamp);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (veid != null ? veid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleEncounter)) {
            return false;
        }
        VehicleEncounter other = (VehicleEncounter) object;
        if ((this.veid == null && other.veid != null) || (this.veid != null && !this.veid.equals(other.veid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eddk2.VehicleEncounter[ veid=" + veid + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public Locations getLid() {
        return lid;
    }

    public void setLid(Locations lid) {
        this.lid = lid;
    }
    
}
